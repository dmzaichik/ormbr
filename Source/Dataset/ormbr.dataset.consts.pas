{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)
  @abstract(Website : http://www.ormbr.com.br)
  @abstract(Telagram : https://t.me/ormbr)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dataset.consts;
{$I ..\ormbr.inc}
interface

const
  cFIELDEVENTS = '%s event required in column [%s]!';
  cNOTFIELDTYPEBLOB = 'Column [%s] must have blob value';
  cCREATEBINDDATASET = 'Access class %s by method %s';

resourcestring
{$ifdef LANG_BR}
  StrIsNotTClientDataSet = 'Is not TClientDataSet type';
  StrIsNotTFDMemTable = 'Is not TFDMemTable type';
  StrNotInInstance = 'Not in instance %s - %s';
  StrColumnError = 'Column : %s';
  StrFieldSingletonWarn = 'Para usar o FieldFactory use o m�todo TFieldSingleton.GetInstance()';
  StrFieldExistCalc = 'O Campo calculado : %s j� existe';
  StrFieldExistAgr = 'O Campo agregado de nome : %s j� existe';
  StrEnableDirective = 'Enable the directive "USEFDMEMTABLE" or "USECLIENTDATASET" in file ormbr.inc';
{$endif}
{$ifDEF LANG_EN}
  StrIsNotTClientDataSet = 'Is not TClientDataSet type';
  StrIsNotTFDMemTable = 'Is not TFDMemTable type';
  StrNotInInstance = 'Not in instance %s - %s';
  StrColumnError = 'Column : %s';
  StrFieldSingletonWarn = 'To use FieldFactory use the TFieldSingleton.GetInstance ()';
  StrFieldExistCalc = 'Calculated field: %s already exists';
  StrFieldExistAgr = 'Aggregate name field:% s already exists';
  StrEnableDirective = 'Enable the directive "USEFDMEMTABLE" or "USECLIENTDATASET" in file ormbr.inc';
{$endif}
{$ifDEF LANG_RU}
  StrIsNotTClientDataSet = 'Is not TClientDataSet type';
  StrIsNotTFDMemTable = 'Is not TFDMemTable type';
  StrNotInInstance = 'Not in instance %s - %s';
  StrColumnError = 'Column : %s';
  StrFieldSingletonWarn = 'To use FieldFactory use the TFieldSingleton.GetInstance ()';
  StrFieldExistCalc = 'Calculated field: %s already exists';
  StrFieldExistAgr = 'Aggregate name field:% s already exists';
  StrEnableDirective = 'Enable the directive "USEFDMEMTABLE" or "USECLIENTDATASET" in file ormbr.inc';
{$endif}
implementation

end.

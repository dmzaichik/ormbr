{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)
  @abstract(Website : http://www.ormbr.com.br)
  @abstract(Telagram : https://t.me/ormbr)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.core.consts;
{$I ..\ormbr.inc}
interface

const
  cENUMERATIONSTYPEERROR = 'Invalid type. Type enumerator supported [ftBoolena, ftInteger, ftFixedChar, ftString]';

resourcestring
{$ifdef LANG_BR}
  StrUtilSingletonWarn = 'Para usar o IUtilSingleton use o m�todo TUtilSingleton.GetInstance()';
  StrRttiSingletonWarn = 'Para usar o IRttiSingleton use o m�todo TRttiSingleton.GetInstance()';
  StrMappingExplorerWarn = 'Para usar o IMappingExplorer use o m�todo TMappingExplorer.GetInstance()';
  StrInvalidOperation = 'Invalid operation, Nullable type has no value.';
  StrCannotAssignedPointer = 'Cannot assigned non-null pointer to nullable type.';
  StrColumnMustHave = 'Column [%s] must have blob value';
  StrInvalidImage = 'Invalid image';
  StrCannotCreateOM = 'O Object Manager n�o deve ser inst�nciada diretamente, use as classes TSessionObject<M> ou TSessionDataSet<M>';
  StrCannotFindPropConstructor = 'Cannot find a propert constructor for %s';
  StrClasseNotRegistrered = 'Classe %s n�o registrada. Registre no Initialization usando TRegisterClasses.GetInstance.RegisterClass(%s)';
  StrFieldNotNull = 'Campo [ %s ] n�o pode ser vazio';
  StrMinimumValueConstraint = 'O valor m�nimo do campo [ %s ] permitido � [ %s ]!';
  StrDefaultExpression = 'O valor Default [ %s ] do campo [ %s ] na classe [ %s ], � inv�lido!';
  StrMaximumValueConstraint = 'O valor m�ximo do campo [ %s ] permitido � [ %s ]!';
  StrFieldValidate = 'Field [ %s ] %s';
  StrEnumUnexpectedValue = 'Enumeration definido "%s" inv�lido para o tipo. Nota: Tipo chars ou strings, defina em mai�sculo.';
  StrErrorMessage = 'Error Message';
  StrAddNameValuesOverArray = 'AddNameValue(%s) over array';
  StrAddValueOverObject = 'AddValue() over object';
  StrUnexpectedValueAccess = 'Unexpected Value[] access';
  StrUnexpectedValue = 'Unexpected Value['''']';
  cERRORDECODINGURLTEXT = 'Error decoding URL style (%%XX) encoded string at position %d';
  cINVALIDURLENCODEDCHAR = 'Invalid URL encoded character (%s) at position %d';
  cINVALIDHTMLENCODEDCHAR = 'Invalid HTML encoded character (%s) at position %d';
  cMEMORYSTREAMERROR = 'Out of memory while expanding memory stream';
  StrDriverNotRegister = 'O driver %s n�o est� registrado, adicione a unit "ormbr.dml.generator.???.pas" onde ??? nome do driver, na cl�usula USES do seu projeto!';
  StrAttributeMissing = 'Falta definir o atributo [Column()] nas propriedades da classe [%s]';
{$endif}
{$ifDEF LANG_EN}
  StrUtilSingletonWarn = 'To use IUtilSingleton, use the TUtilSingleton.GetInstance ()';
  StrRttiSingletonWarn = 'To use IRttiSingleton, use the TRttiSingleton.GetInstance ()';
  StrMappingExplorerWarn = 'To use IMappingExplorer use the method TMappingExplorer.GetInstance ()';
  StrInvalidOperation = 'Invalid operation, Nullable type has no value.';
  StrCannotAssignedPointer = 'Cannot assigned non-null pointer to nullable type.';
  StrColumnMustHave = 'Column [%s] must have blob value';
  StrInvalidImage = 'Invalid image';
  StrCannotCreateOM = 'The Object Manager should not be instantiated directly, use the TSessionObject <M> or TSessionDataSet <M>';
  StrCannotFindPropConstructor = 'Cannot find a propert constructor for %s';
  StrClasseNotRegistrered = 'Class %s not registered. Register in Initialization using TRegisterClasses.GetInstance.RegisterClass (%s)';
  StrFieldNotNull = 'Field [%s] cannot be empty';
  StrMinimumValueConstraint = 'Minimum value of the field [%s] allowed [%s]!';
  StrDefaultExpression = 'Default value [%s] of field [%s] in class [%s], is invalid!';
  StrMaximumValueConstraint = 'Maximum value of the field [%s] allowed [%s]!';
  StrFieldValidate = 'Field [ %s ] %s';
  StrEnumUnexpectedValue = 'Enumeration set to "%s" invested for type. Note: Type chars or strings, set to minus.';
  StrErrorMessage = 'Error Message';
  StrAddNameValuesOverArray = 'AddNameValue(%s) over array';
  StrAddValueOverObject = 'AddValue() over object';
  StrUnexpectedValueAccess = 'Unexpected Value[] access';
  StrUnexpectedValue = 'Unexpected Value['''']';
  cERRORDECODINGURLTEXT = 'Error decoding URL style (%%XX) encoded string at position %d';
  cINVALIDURLENCODEDCHAR = 'Invalid URL encoded character (%s) at position %d';
  cINVALIDHTMLENCODEDCHAR = 'Invalid HTML encoded character (%s) at position %d';
  cMEMORYSTREAMERROR = 'Out of memory while expanding memory stream';
  StrDriverNotRegister = 'The driver %s is not registered, add the unit "ormbr.dml.generator.???.pas" where ??? name of the driver, in the USES clause of your project!';
  StrAttributeMissing = 'The [Column ()] attribute is missing in the properties of the class [%s]';
{$endif}
{$ifDEF LANG_RU}
  StrUtilSingletonWarn = 'To use IUtilSingleton, use the TUtilSingleton.GetInstance ()';
  StrRttiSingletonWarn = 'To use IRttiSingleton, use the TRttiSingleton.GetInstance ()';
  StrMappingExplorerWarn = 'To use IMappingExplorer use the method TMappingExplorer.GetInstance ()';
  StrInvalidOperation = 'Invalid operation, Nullable type has no value.';
  StrCannotAssignedPointer = 'Cannot assigned non-null pointer to nullable type.';
  StrColumnMustHave = 'Column [%s] must have blob value';
  StrInvalidImage = 'Invalid image';
  StrCannotCreateOM = 'The Object Manager should not be instantiated directly, use the TSessionObject <M> or TSessionDataSet <M>';
  StrCannotFindPropConstructor = 'Cannot find a propert constructor for %s';
  StrClasseNotRegistrered = 'Class %s not registered. Register in Initialization using TRegisterClasses.GetInstance.RegisterClass (%s)';
  StrFieldNotNull = 'Field [%s] cannot be empty';
  StrMinimumValueConstraint = 'Minimum value of the field [%s] allowed [%s]!';
  StrDefaultExpression = 'Default value [%s] of field [%s] in class [%s], is invalid!';
  StrMaximumValueConstraint = 'Maximum value of the field [%s] allowed [%s]!';
  StrFieldValidate = 'Field [ %s ] %s';
  StrEnumUnexpectedValue = 'Enumeration set to "%s" invested for type. Note: Type chars or strings, set to minus.';
  StrErrorMessage = 'Error Message';
  StrAddNameValuesOverArray = 'AddNameValue(%s) over array';
  StrAddValueOverObject = 'AddValue() over object';
  StrUnexpectedValueAccess = 'Unexpected Value[] access';
  StrUnexpectedValue = 'Unexpected Value['''']';
  cERRORDECODINGURLTEXT = 'Error decoding URL style (%%XX) encoded string at position %d';
  cINVALIDURLENCODEDCHAR = 'Invalid URL encoded character (%s) at position %d';
  cINVALIDHTMLENCODEDCHAR = 'Invalid HTML encoded character (%s) at position %d';
  cMEMORYSTREAMERROR = 'Out of memory while expanding memory stream';
  StrDriverNotRegister = 'The driver %s is not registered, add the unit "ormbr.dml.generator.???.pas" where ??? name of the driver, in the USES clause of your project!';
  StrAttributeMissing = 'The [Column ()] attribute is missing in the properties of the class [%s]';
{$endif}

implementation

end.
